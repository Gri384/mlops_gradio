from fastapi import FastAPI
import gradio as gr
import random

CUSTOM_PATH = "/predict"

app = FastAPI()


@app.get("/")
def read_main():
    return {"message": "This is your main app"}


def echo(text, request: gr.Request):
    if request:
        print("Request headers dictionary:", request.headers)
        print("IP address:", request.client.host)
        print("Query parameters:", dict(request.query_params))
        kek = random.choice(['positive','negative'])
    return kek

io = gr.Interface(echo, "textbox", "textbox")

app = gr.mount_gradio_app(app, io, path=CUSTOM_PATH)
