# Домашнее задание MLOps - Gradio.

Клонируем репозиторий, выполняем команду

```
    poetry run uvicorn main:app
```

Открываем вкладку [http://localhost:8000/predict](http://localhost:8000/predict)
Вводим текст и нажимаем **Submit**

Пока модели нет, тональность определяется случайно и выводится в окне **output**.
